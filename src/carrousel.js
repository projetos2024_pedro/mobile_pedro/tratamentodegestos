import React, { useRef, useState } from "react";
import {
  View,
  Text,
  Dimensions,
  PanResponder,
  Image,
  StyleSheet,
} from "react-native";

    const images = [
        "https://cdn.meutimao.com.br/_upload/noticia/2024/03/05/matias-rojas-acerta-com-time-de-messi-apos-3h941w.jpg",
        "https://s2-ge.glbimg.com/sayuLYM8EhSCcRlGtqNR8_HPcxo=/0x0:2695x3000/984x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_bc8228b6673f488aa253bbcb03c80ec5/internal_photos/bs/2024/c/q/5F3sCLQIS36OEOmPu3mQ/raniele-corinthians.jpg",
        "https://p2.trrsf.com/image/fget/cf/774/0/images.terra.com/2024/03/02/1620503219-yuri-e1709417493386.jpg",
    ];

export default function CarrouselMoviment() {
  const [image, setImage] = useState(0);
  const screenWidth = Dimensions.get("window").width;
  const gestureThreshold = screenWidth * 0.1;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dx < -gestureThreshold) {
          setImage((prevImage) => (prevImage > 0 ? prevImage +1 : prevImage));
        } else if (gestureState.dx > gestureThreshold) {
          setImage((prevImage) =>
            prevImage < images.length - 1 ? prevImage + 1 : prevImage
          );
        }
      },
    })
  ).current;

  return (
    <View {...panResponder.panHandlers} style={styles.container}>
      <Image source={{ uri: images[image] }} style={styles.image} />
      <Text style={styles.text}>Imagem {image + 1}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f0f0f0",
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: "cover",
    marginBottom: 20,
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#333",
  },
});
