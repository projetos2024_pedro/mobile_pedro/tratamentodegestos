import React from 'react';
import { View, Button, TouchableOpacity, Text, StyleSheet} from "react-native";
import { useNavigation } from '@react-navigation/native'

const HomeScreen = () => {

    const navigation = useNavigation();

    const handleTratamentoGestos = () => {
        navigation.navigate('CountMoviment')
    }
    const handleCaroussel = () => {
        navigation.navigate('CarrouselMoviment')
    }

    return(
        <View style={styles.container}>
            <TouchableOpacity style={styles.touchable} onPress={handleTratamentoGestos}>
                <Text style={styles.textTouchable}>Tratamento de Gestos</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchable} onPress={handleCaroussel}>
                <Text style={styles.textTouchable}>Carroussel de Imagens</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textTouchable: {
        fontWeight: '500', 
        textAlign: 'center',
        fontSize: 18
    },  
    touchable: {
        margin: 'auto',
        borderWidth: 1,
        borderColor: '#7A7A7A',
        width: 250, 
        maxHeight: 40,
        borderRadius: 3, 
        padding: 3,
        marginTop: 10
    },
});
export default HomeScreen;