import React, { useRef, useState, useEffect } from "react";
import {
  View,
  Dimensions,
  PanResponder,
  TouchableOpacity,
  Text,
} from "react-native";

const CountMoviment = () => {
  const [count, setCount] = useState(0);
  const screenHeight = Dimensions.get("window").height;
  const gestureThreshould = screenHeight * 0.25;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy < -gestureThreshould) {
          setCount((prevCount) => prevCount + 1);
        }
      },
    })
  ).current;
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} {...panResponder.panHandlers}>
      <Text>Valor do Contador: {count}</Text>
    </View>
  );
};

export default CountMoviment;
